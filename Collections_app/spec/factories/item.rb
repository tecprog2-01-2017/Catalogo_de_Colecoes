FactoryGirl.define do
  factory :item, class: Item do

    association :collection, factory: :collection_valid

    factory :item_valid, class: Item do
      name {"Item name"}
      description {"Item decription"}
      picture {"Item picture"}
    end

    factory :item_new_valid, class: Item do
      name {"New Item name"}
      description {"New Item decription"}
      picture {"New Item picture"}
    end
  end
end
