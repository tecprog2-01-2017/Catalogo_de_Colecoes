FactoryGirl.define do
  factory :collection, class: Collection do

    association :company, factory: :company_valid

    factory :collection_valid do
      name {"Collection name"}
      description {"Collection decription"}
      begin_date {"10-10-2010"}
      coll_count {1}
    end

    factory :collection_new_valid do
      name {"New Collection name"}
      description {"New Collection decription"}
      begin_date {"11-11-2011"}
      coll_count {2}
    end

    factory :collection_invalid do
      name {""}
      description {""}
      begin_date {"11-11-2011"}
      coll_count {1}
    end
  end
end
