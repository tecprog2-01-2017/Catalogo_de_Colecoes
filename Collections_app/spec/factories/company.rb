FactoryGirl.define do
  factory :company_valid, class: Company do
    name "Company name"
    profile "Company profile"
    cnpj "Company Cnpj"
    association :user, factory: :user_company_valid
  end
end
