FactoryGirl.define do
  factory :user_company, class: User do

    username "Company"
    email "company@company.com"
    level "company"

    factory :user_company_valid, class: User do
      password "Password"
    end

    factory :user_company_new_valid, class: User do
      password "NewPassword"
    end
  end
  factory :user_collector, class: User do
    username "Collector"
    email "collector@collector.com"
    level "collector"

    factory :user_collector_valid, class: User do
      password "Password"
    end

    factory :user_collector_new_valid, class: User do
      password "NewPassword"
    end
  end
end
