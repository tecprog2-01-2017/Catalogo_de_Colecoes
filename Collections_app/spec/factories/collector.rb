FactoryGirl.define do
  factory :collector_valid, class: Collector do
    name "Collector name"
    avatar "Collector avatar"
    association :user, factory: :user_collector_valid
  end
end
