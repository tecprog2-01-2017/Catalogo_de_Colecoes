require 'rails_helper'

# This are the tests for the collection controller, they were automaticaly
# generated. However we made a few changes to better adept to our needs
# Since tests are still something new to us, we kept the instructions from
# the rails generator, don't worry we'll get rid of them soon

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

RSpec.describe CollectionsController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Collection. As you add validations to Collection, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    attributes_for(:collection_valid)
  }

  let(:invalid_attributes) {
    attributes_for(:collection_invalid)
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # CollectionsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "assigns all collections as @collections" do
      collection = create(:collection_valid)
      get :index, params: {}, session: valid_session
      expect(assigns(:collections)).to eq([collection])
    end
  end

  describe "GET #show" do
    it "assigns the requested collection as @collection" do
      collection = create(:collection_valid)
      get :show, params: {id: collection.to_param}, session: valid_session
      expect(assigns(:collection)).to eq(collection)
    end
  end

  describe "GET #new" do
    it "assigns a new collection as @collection" do
      get :new, params: {}, session: valid_session
      expect(assigns(:collection)).to be_a_new(Collection)
    end
  end

  describe "GET #edit" do
    it "assigns the requested collection as @collection" do
      collection = create(:collection_valid)
      get :edit, params: {id: collection.to_param}, session: valid_session
      expect(assigns(:collection)).to eq(collection)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Collection" do
        user = create(:user_company_valid)
        sign_in user
#        puts "="*100, attributes_for(:collection_valid)
        expect {
          process :create, method: :post, params: {collection: attributes_for(:collection_valid)}, session: valid_session
        }.to change(Collection, :count).by(1)
      end

      it "assigns a newly created collection as @collection" do
        user = create(:user_company_valid)
        sign_in user
        process :create, method: :post, params: {collection: attributes_for(:collection_valid, company: create(:company_valid, user: user))}, session: valid_session
        expect(assigns(:collection)).to be_a(Collection)
        expect(assigns(:collection)).to be_persisted
      end

      it "redirects to the created collection" do
        post :create, params: {collection: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Collection.last)
      end
    end

#    context "with invalid params" do
#      it "assigns a newly created but unsaved collection as @collection" do
#        skip ("dealing with invalid params")
#        post :create, params: {collection: invalid_attributes}, session: valid_session
#        expect(assigns(:collection)).to be_a_new(Collection)
#      end

#      it "re-renders the 'new' template" do
#        skip ("dealing with invalid params")
#        post :create, params: {collection: invalid_attributes}, session: valid_session
#        expect(response).to render_template("new")
#      end
#    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        attrs = attributes_for(:collection_new_valid)
      }

      it "updates the requested collection" do
        user = create(:user_company_valid)
        sign_in user
        collection = create(:collection_valid, company: create(:company_valid, user: user))
        put :update, params: {id: collection.to_param, collection: new_attributes, company: create(:company_valid, user: user)}, session: valid_session
        collection.reload
        collection.coll_count == 2
      end

      it "assigns the requested collection as @collection" do
        user = create(:user_company_valid)
        sign_in user
        collection = create(:collection_valid, company: create(:company_valid, user: user))
        put :update, params: {id: collection.to_param, collection: new_attributes}, session: valid_session
        expect(assigns(:collection)).to eq(collection)
      end

      it "redirects to the collection" do
        user = create(:user_company_valid)
        sign_in user
        collection = create(:collection_valid, company: create(:company_valid, user: user))
        put :update, params: {id: collection.to_param, collection: new_attributes}, session: valid_session
        expect(response).to redirect_to(collection)
      end
    end

#    context "with invalid params" do
#      it "assigns the collection as @collection" do
#        skip("Dealing with invalid params is yet to be implemented! Coming soon, in phase 4! (b'u')b")
#        collection = create(:collection_valid)
#        put :update, params: {id: collection.to_param, collection: invalid_attributes}, session: valid_session
#        expect(assigns(:collection)).to eq(collection)
#      end

#      it "re-renders the 'edit' template" do
#        skip("Dealing with invalid params is yet to be implemented! Coming soon, in phase 4! (b'u')b")
#        collection = create(:collection_valid)
#        put :update, params: {id: collection.to_param, collection: invalid_attributes}, session: valid_session
#        expect(response).to render_template("edit")
#      end
#    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested collection" do
      user = create(:user_company_valid)
      sign_in user
      collection = create(:collection_valid, company: create(:company_valid, user: user))
      expect {
        process :destroy, method: :delete, params: {id: collection.to_param, collection: valid_attributes, company: create(:company_valid, user: user)}, session: valid_session
      }.to change(Collection, :count).by(-1)
    end

    it "redirects to the collections list" do
      collection = create(:collection_valid)
      delete :destroy, params: {id: collection.to_param}, session: valid_session
      expect(response).to redirect_to(collections_url)
    end
  end

end
