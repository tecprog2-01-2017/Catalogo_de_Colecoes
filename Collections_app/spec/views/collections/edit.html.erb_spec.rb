require 'rails_helper'

# Simple test of the collection edit page
# PATCH/PUT /collections/1

RSpec.describe "collections/edit", type: :view do
  before(:each) do
    @collection = assign(:collection, create(:collection_valid))
  end

  it "renders the edit collection form" do
    render

    assert_select "form[action=?][method=?]", collection_path(@collection), "post" do
    end
  end
end
