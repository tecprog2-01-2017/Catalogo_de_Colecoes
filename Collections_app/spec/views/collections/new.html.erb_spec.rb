require 'rails_helper'

# Simple test of the collection new page
# POST /collections

RSpec.describe "collections/new", type: :view do
  before(:each) do
    assign(:collection, Collection.new())
  end

  it "renders new collection form" do
    render

    assert_select "form[action=?][method=?]", collections_path, "post" do
    end
  end
end
