require 'rails_helper'

RSpec.describe "items/edit", type: :view do
  before(:each) do
    @collection = assign(:collection, create(:collection_valid))
    @item = assign(:item, create(:item_valid, collection: @collection))
  end

  it "renders the edit item form" do
    render

    assert_select "form[action=?][method=?]", item_path(@item), "post" do
    end
  end
end
