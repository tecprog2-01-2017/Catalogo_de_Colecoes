require 'rails_helper'

RSpec.describe "items/index", type: :view do

  before(:each) do
    user = FactoryGirl.create(:user_company_valid)
    sign_in user
    assign(:items, [
      create(:item_valid, collection: create(:collection_valid, company: create(:company_valid, user: user)))
    ])
  end

  it "renders a list of items" do
    render
  end
end
