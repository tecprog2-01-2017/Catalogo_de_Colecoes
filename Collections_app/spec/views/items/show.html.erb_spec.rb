require 'rails_helper'

RSpec.describe "items/show", type: :view do

  before(:each) do
    user = FactoryGirl.create(:user_company_valid)
    sign_in user
    assign(:item, create(:item_valid, collection: create(:collection_valid, company: create(:company_valid, user: user))))
  end

  it "renders attributes in <p>" do
    render
  end
end
