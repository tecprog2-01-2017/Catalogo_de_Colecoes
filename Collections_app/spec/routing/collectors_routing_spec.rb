require "rails_helper"

RSpec.describe CollectorsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/collectors/index").to route_to("collectors#index")
    end

    it "routes to #show" do
      expect(:get => "/collectors/show").to route_to("collectors#show")
    end

    it "routes to #add_item" do
      expect(:get => "/collectors/add_item").to route_to("collectors#add_item")
    end

    it "routes to #remove_item" do
      expect(:get => "/collectors/remove_item").to route_to("collectors#remove_item")
    end

    it "routes to #unfollow_collection" do
      expect(:get => "/collectors/unfollow_collection").to route_to("collectors#unfollow_collection")
    end

  end
end
