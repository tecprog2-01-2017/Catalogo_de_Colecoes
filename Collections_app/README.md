# Para rodar a aplicação localmente:

ruby >= 2.4.0
gem install bundler
bundle install

## Iniciar o banco de dados
rake db:migrate

## Funcionalidade

O estado atual do cadastro não reflete o estado final almejado.

No campo "level", escreva "company" para acessar todas as funcionalidades do
programa com facilidade.
