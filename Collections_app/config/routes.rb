Rails.application.routes.draw do

  resources :items
  resources :collections
  resources :companies
  resources :ratings, only: :update
  devise_for :users
  root 'home#index'

  # Talvez colocar aquele esquema de rota especial de login (de logout não precisa), isso no application_controller
  # Para isso foi criado o home/show.html.erb

  get 'collectors/index'
  get 'collectors/show'
  get 'collectors/add_item'
  get 'collectors/remove_item'
  get 'collectors/unfollow_collection'
  get 'companies/index'
  get 'companies/show'
  get "user", to: "login#user"

end
