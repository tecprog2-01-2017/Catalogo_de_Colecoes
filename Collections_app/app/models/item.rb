class Item < ApplicationRecord
  belongs_to :collection, :foreign_key => :collection_id, :class_name => "Collection"
  has_many :collector_items
  has_many :collectors, :through => :collector_items
  validates :name, :description, presence: true
  has_many :ratings

  has_attached_file :avatar, styles: { medium: "300x300", thumb: "100x100" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  # Validate filename
  validates_attachment_file_name :avatar, matches: [/png\Z/, /jpe?g\Z/]
  # Explicitly do not validate
  do_not_validate_attachment_file_type :avatar

  def average_rating
    if ratings.size > 0
      ratings.sum(:score) / ratings.size
    else
      return 0
    end
  end
end
