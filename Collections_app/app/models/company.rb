class Company < ApplicationRecord
  belongs_to :user
  has_many :collections

  def average_rating
    sum = 0
    zeros = 0;
    collections.each do |collection|
      if collection.average_rating == 0
        zeros += 1
      end
      sum += collection.average_rating
    end
    if collections.count == zeros
      return 0
    end
    return  sum / (collections.count - zeros)
  end
end
