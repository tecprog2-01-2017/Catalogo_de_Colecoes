class Collection < ApplicationRecord
  has_many :items
  belongs_to :company
  validates :name, :description, presence: true


  has_attached_file :avatar, styles: { medium: "300x300", thumb: "100x100" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  # Validate filename
  validates_attachment_file_name :avatar, matches: [/png\Z/, /jpe?g\Z/]
  # Explicitly do not validate
  do_not_validate_attachment_file_type :avatar
  
  def average_rating
    sum = 0
    zeros = 0;
    if items.count == 0
      return 0
    end
    items.each do |item|
      if item.average_rating == 0
        zeros += 1
      end
      sum += item.average_rating
    end
    if items.count == zeros
      return 0
    end
    return  sum / (items.count - zeros)
  end
  
  def collectors_count
    item_ids = self.items.pluck :id # Recebe os IDs dos itens da coleção em um array
    collector_ids = CollectorItem.where("item_id in (?)", item_ids).pluck(:collector_id).uniq # Recebe os IDs dos colecionadores dos itens da coleção sem repetição
    # Collector.where("id in (?)", collector_ids)
    return collector_ids.size
  end
end
