class CollectorItem < ApplicationRecord
  belongs_to :collector
  belongs_to :item
end
