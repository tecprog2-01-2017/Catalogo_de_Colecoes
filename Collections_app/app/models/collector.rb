class Collector < ApplicationRecord
  belongs_to :user
  has_many :collector_items
  has_many :items, :through => :collector_items
  has_many :ratings
end
