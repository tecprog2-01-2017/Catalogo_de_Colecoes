class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable

	validates :email,	 :format => {:with => /.+@.+/i}
	validates :username, presence: true, uniqueness: true
	#validates :password, length: { minimum: 6 }
  has_one :company
  has_one :collector

  after_create :define_type

	has_attached_file :avatar, styles: { medium: "300x300", thumb: "100x100" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  # Validate filename
  validates_attachment_file_name :avatar, matches: [/png\Z/, /jpe?g\Z/]
  # Explicitly do not validate
  do_not_validate_attachment_file_type :avatar

  def define_type
    #puts "*"*100 + self.level
    if self.level == "collector"
      collector = ::Collector.new
      collector.name = self.username
      collector.user = self
      collector.save!
    else
      company = Company.new
      company.name = self.username
      company.user = self
      company.save!
    end
  end


  def collector?
     return !self.collector.nil?
  end
  def company?
    return !self.company.nil?
  end
end
