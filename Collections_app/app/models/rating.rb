class Rating < ApplicationRecord
  belongs_to :item
  belongs_to :collector
end
