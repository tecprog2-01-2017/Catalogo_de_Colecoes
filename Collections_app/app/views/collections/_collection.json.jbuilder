json.extract! collection, :id, :name, :begin_date, :description, :coll_count, :created_at, :updated_at
json.url collection_url(collection, format: :json)
