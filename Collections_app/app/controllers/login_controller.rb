class LoginController < ApplicationController
  def login
  end

  def user
    if user_signed_in?
      @user = User.find_ by_email(params["email"])
      if @user.company?
        redirect_to controller: "companies", action: "show", params: {id: @user.company.id}
      elsif @user.collector?
        redirect_to controller: "collectors", action: "show", params: {id: @user.collector.id}
      end
    else
      redirect_to controller: "home", action: "index"
    end

  end
end
