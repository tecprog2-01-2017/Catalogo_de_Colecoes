class HomeController < ApplicationController

  # GET /controllers
  # GET /controllers.json
  def index
    if user_signed_in?
      if current_user.company?
        @collections = current_user.company.collections
      else
        @collections = []
        current_user.collector.items.each do |item|
         @collections << item.collection
        end
        @collections.uniq!
      end
    end
  end
end
