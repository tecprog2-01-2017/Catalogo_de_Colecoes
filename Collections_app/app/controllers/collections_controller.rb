class CollectionsController < ApplicationController
  before_action :set_collection, only: [:show, :edit, :update, :destroy]

  # GET /collections
  # GET /collections.json
  def index
    @collections = Collection.all
  end

  def self.search(search)
    where("name LIKE ?", "%#{search}%")
  end

  # GET /collections/1
  # GET /collections/1.json
  def show
  end

  # GET /collections/new
  def new
    @collection = Collection.new
  end

  # GET /collections/1/edit
  def edit
    if !user_signed_in?
      redirect_to controller: "collections", action: "index"
    end
  end

  # POST /collections
  # POST /collections.json
  def create
    if user_signed_in?
      @collection = Collection.new(collection_params)
      @collection.company = current_user.company
      respond_to do |format|
        if @collection.save
          format.html { redirect_to @collection, notice: 'Collection was successfully created.' }
          format.json { render :show, status: :created, location: @collection }
        else
          format.html { render :new }
          format.json { render json: @collection.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to controller: "collections", action: "index"
    end
  end

  # PATCH/PUT /collections/1
  # PATCH/PUT /collections/1.json
  def update
    if user_signed_in?
      respond_to do |format|
        if @collection.update(collection_params)
          format.html { redirect_to @collection, notice: 'Collection was successfully updated.' }
          format.json { render :show, status: :ok, location: @collection }
        else
          format.html { render :edit }
          format.json { render json: @collection.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to controller: "collections", action: "index"
    end
  end

  # DELETE /collections/1
  # DELETE /collections/1.json
  def destroy
    if user_signed_in?
      @collection.destroy
      respond_to do |format|
        format.html { redirect_to root_path, notice: 'Collection "' + @collection.name + '" was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to controller: "collections", action: "index"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collection
      @collection = Collection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def collection_params
      params.require(:collection).permit(:name, :begin_date, :description, :coll_count, :avatar, :avatar_cache)
    end
end
