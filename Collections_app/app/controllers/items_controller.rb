class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  # GET /items
  # GET /items.json
  def index
    @items = Item.all
  end

  # GET /items/1
  # GET /items/1.json
  def show
    if user_signed_in? && current_user.collector?
      @rating = Rating.where(item_id: @item.id, collector_id: current_user.collector.id).first
      unless @rating
        @rating = Rating.create(item_id: @item.id, collector_id: current_user.collector.id, score: 0)
      end
    end
  end

  # GET /items/new
  def new
    if user_signed_in?
      @item = Item.new
      @collection = Collection.find(params[:collection])
    else
      redirect_to controller: "items", action: "index"
    end
  end

  # GET /items/1/edit
  def edit
    if user_signed_in?
      @collection = Collection.find(params[:collection])
    else
      redirect_to controller: "items", action: "index"
    end
  end

  # POST /items
  # POST /items.json
  def create
    if user_signed_in?
      @item = Item.new(item_params)
      respond_to do |format|
        if @item.save
          format.html { redirect_to @item, notice: 'Item was successfully created.' }
          format.json { render :show, status: :created, location: @item }
        else
          @collection = Collection.find(params[:item][:collection_id])
          format.html { render :new }
          format.json { render json: @item.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to controller: "items", action: "index"
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    if user_signed_in?
      respond_to do |format|
        if @item.update(item_params)
          format.html { redirect_to @item, notice: 'Item was successfully updated.' }
          format.json { render :show, status: :ok, location: @item }
        else
          @collection = Collection.find(params[:item][:collection_id])
          format.html { render :edit }
          format.json { render json: @item.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to controller: "items", action: "index"
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    if user_signed_in?
      @item.destroy
      respond_to do |format|
        format.html { redirect_to collection_url(@item.collection_id), notice: 'Item was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to controller: "items", action: "index"
    end
  end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_item
        @item = Item.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def item_params
        params.require(:item).permit(:name, :description, :collection_id, :avatar, :avatar_cache)
      end
  end
