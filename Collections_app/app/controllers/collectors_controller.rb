class CollectorsController < ApplicationController
  def index
    @collectors = ::Collector.all
  end


  def show
    @collections = []
    @collector = ::Collector.find_by_id(params[:collector])
    @collector.items.each do |item|
      @collections.append(item.collection)
    end
    @collections.uniq!
  end

  def add_item
    item =  Item.find_by_id(params[:item])
    current_user.collector.collector_items.create(item: item)
    redirect_to collection_path(item.collection)
  end

  def remove_item
    item =  Item.find_by_id(params[:item])
    c = CollectorItem.find_by(item: item)
    c.destroy!
    redirect_to collection_path(item.collection)
  end

  def unfollow_collection
    current_user.collector.items.each do |item|
      if item.collection.id == params[:collection].to_i
        CollectorItem.find_by(item: item, collector: current_user.collector).destroy!
      end
    end
    redirect_to root_path
  end
end
