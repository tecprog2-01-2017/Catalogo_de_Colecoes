class AddAvatarToCollection < ActiveRecord::Migration[5.0]
  def up
    add_attachment :collections, :avatar
  end

  def down
    remove_attachment :collections, :avatar
  end
end
