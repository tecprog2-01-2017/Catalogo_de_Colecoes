class CreateCollections < ActiveRecord::Migration[5.0]
  def change
    create_table :collections do |t|
      t.string :name
      t.date :begin_date
      t.string :description
      t.integer :coll_count

      t.timestamps
    end
  end
end
