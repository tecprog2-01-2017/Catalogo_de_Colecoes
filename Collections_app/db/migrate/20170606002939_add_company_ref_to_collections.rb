class AddCompanyRefToCollections < ActiveRecord::Migration[5.0]
  def change
    add_reference :collections, :company, foreign_key: true
  end
end
