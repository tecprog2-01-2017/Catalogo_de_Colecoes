class AddCollectionToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :collection_id, :integer
  end
end
