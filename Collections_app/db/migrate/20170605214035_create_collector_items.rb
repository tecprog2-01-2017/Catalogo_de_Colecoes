class CreateCollectorItems < ActiveRecord::Migration[5.0]
  def change
    create_table :collector_items do |t|
      t.references :collector, foreign_key: true
      t.references :item, foreign_key: true

      t.timestamps
    end
  end
end
