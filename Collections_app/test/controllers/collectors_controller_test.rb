require 'test_helper'

class CollectorsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get collectors_index_url
    assert_response :success
  end

  test "should get show" do
    get collectors_show_url
    assert_response :success
  end

end
